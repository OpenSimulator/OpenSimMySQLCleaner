﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenSimMySQLCleaner
{
    [Serializable()]
    [XmlRoot]
    public class SceneObjectGroup
    {
        [XmlElement("RootPart", typeof(RootPartElement))]
        public RootPartElement RootPart { get; set; }

        [XmlArray("OtherParts")]
        [XmlArrayItem("Part")]
        public PartElement[] OtherParts { get; set; }
    }

    [XmlRoot]
    public class RootPartElement
    {
        [XmlElement("SceneObjectPart", typeof(SceneObjectPartElement))]
        public SceneObjectPartElement SceneObjectPart { get; set; }
    }

    [XmlRoot]
    public class PartElement
    {
        [XmlElement("SceneObjectPart", typeof(SceneObjectPartElement))]
        public SceneObjectPartElement SceneObjectPart { get; set; }
    }

    [XmlRoot]
    public class SceneObjectPartElement
    {
        [XmlElement("Name", typeof(String))]
        public String Name { get; set; }

        [XmlElement("Shape", typeof(ShapeElement))]
        public ShapeElement Shape { get; set; }

        [XmlElement("CollisionSound", typeof(UUIDEntry))]
        public UUIDEntry CollisionSound { get; set; }

        [XmlElement("SoundID", typeof(UUIDEntry))]
        public UUIDEntry SoundID { get; set; }

        [XmlArray("TaskInventory")]
        [XmlArrayItem("TaskInventoryItem")]
        public List<TaskInventoryItemElement> TaskInventory { get; set; }
    }

    [XmlRoot]
    public class TaskInventoryItemElement
    {
        [XmlElement("Name", typeof(String))]
        public String Name { get; set; }

        [XmlElement("AssetID", typeof(UUIDEntry))]
        public UUIDEntry AssetID { get; set; }
    }

    [XmlRoot]
    public class ShapeElement
    {
        [XmlElement("TextureEntry", typeof(String))]
        public String TextureEntry { get; set; }

        [XmlElement("ExtraParams", typeof(String))]
        public String ExtraParams { get; set; }

        [XmlElement("SculptTexture", typeof(UUIDEntry))]
        public UUIDEntry SculptTexture { get; set; }
    }

    [XmlRoot]
    public class UUIDEntry
    {
        [XmlElement("UUID", typeof(String))]
        public String UUID { get; set; }
    }
}
