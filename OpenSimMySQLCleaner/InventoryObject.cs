﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenSimMySQLCleaner
{
    class InventoryObject
    {
        public String InventarID = String.Empty;
        public String AssetID = String.Empty;
        public String Name = String.Empty;
        public String Parent = String.Empty;

        public InventoryObject(String _inventarID, String _assetID, String _name, String _parent)
        {
            InventarID = _inventarID;
            AssetID = _assetID;
            Name = _name;
            Parent = _parent;
        }
    }
}
