﻿using MySql.Data.MySqlClient;
using OpenMetaverse;
using OpenMetaverse.Assets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace OpenSimMySQLCleaner
{
    class MySQLClient
    {
        //private static string m_mySQLConnectionString = "SERVER=rs1.chris-latza.eu;DATABASE=chrisweymann;UID=chrisweymann;PASSWORD=ba84YI8EVu;";
        private static string m_mySQLConnectionString = "SERVER=127.0.0.1;DATABASE=chrisweymann;UID=chrisweymann;PASSWORD=ba84YI8EVu;";
        private static MySqlConnection m_mySQLConnection = new MySqlConnection(m_mySQLConnectionString);

        public static void connect()
        {
            m_mySQLConnection.Open();
        }

        public static List<AssetObject> getAllAssetObjects()
        {
            Console.WriteLine("Start get all asset objects from mysql");
            List<AssetObject> _assets = new List<AssetObject>();

            MySqlCommand _mysqlCommand = m_mySQLConnection.CreateCommand();
            _mysqlCommand.CommandTimeout = int.MaxValue;
            _mysqlCommand.CommandText = "SELECT id,name,assetType FROM assets";

            MySqlDataReader _commandReader = _mysqlCommand.ExecuteReader();

            while(_commandReader.Read())
            {
                String _id = _commandReader.GetString(0);
                String _name = _commandReader.GetString(1);
                int _typ = _commandReader.GetInt32(2);

                _assets.Add(new AssetObject(_id, _name, _typ));
                Console.WriteLine("Found AssetObject: " + _id + " - ( " + _assets.Count + " )");
            }

            _commandReader.Close();

            return _assets;
        }

        public static List<InventoryObject> getAllInventoryObjects()
        {
            Console.WriteLine("Start get all inventar objects from mysql");
            List<InventoryObject> _inventorys = new List<InventoryObject>();

            MySqlCommand _mysqlCommand = m_mySQLConnection.CreateCommand();
            _mysqlCommand.CommandText = "SELECT inventoryID,assetID,inventoryName,parentFolderID FROM inventoryitems";
            _mysqlCommand.CommandTimeout = int.MaxValue;

            MySqlDataReader _commandReader = _mysqlCommand.ExecuteReader();

            while (_commandReader.Read())
            {
                String _inventoryID = _commandReader.GetString(0);
                String _assetID = _commandReader.GetString(1);
                String _name = _commandReader.GetString(2);
                String _parent = _commandReader.GetString(3);

                _inventorys.Add(new InventoryObject(_inventoryID, _assetID, _name, _parent));
                Console.WriteLine("Found InventoryObject: " + _assetID + " ( "+ _inventorys.Count + " )");
            }

            _commandReader.Close();

            return _inventorys;
        }

        public static List<InventoryFolderObject> getAllInventoryFolders()
        {
            Console.WriteLine("Start get all inventar folders from mysql");
            List<InventoryFolderObject> _inventorys = new List<InventoryFolderObject>();

            MySqlCommand _mysqlCommand = m_mySQLConnection.CreateCommand();
            _mysqlCommand.CommandText = "SELECT folderID,parentFolderID,agentID FROM inventoryfolders";
            _mysqlCommand.CommandTimeout = int.MaxValue;

            MySqlDataReader _commandReader = _mysqlCommand.ExecuteReader();

            while (_commandReader.Read())
            {
                String _folderID = _commandReader.GetString(0);
                String _parentFolderID = _commandReader.GetString(1);
                String _agentID = _commandReader.GetString(2);

                _inventorys.Add(new InventoryFolderObject(_folderID, _parentFolderID, _agentID));
                Console.WriteLine("Found InventoryFolder: " + _folderID + " ( " + _inventorys.Count + " )");
            }

            _commandReader.Close();

            return _inventorys;
        }

        public static List<UserAccountObject> getAllGridUsers()
        {
            Console.WriteLine("Start get all grid users from mysql");
            List<UserAccountObject> _avatars = new List<UserAccountObject>();

            MySqlCommand _mysqlCommand = m_mySQLConnection.CreateCommand();
            _mysqlCommand.CommandText = "SELECT PrincipalID FROM useraccounts";
            _mysqlCommand.CommandTimeout = int.MaxValue;

            MySqlDataReader _commandReader = _mysqlCommand.ExecuteReader();

            while (_commandReader.Read())
            {
                String _agentID = _commandReader.GetString(0);

                _avatars.Add(new UserAccountObject(_agentID));
                Console.WriteLine("Found Agent: " + _agentID + " ( " + _avatars.Count + " )");
            }

            _commandReader.Close();

            return _avatars;
        }

        public static String getAssetData(String _id)
        {
            String _data = null;

            MySqlCommand _mysqlCommand = m_mySQLConnection.CreateCommand();
            _mysqlCommand.CommandTimeout = int.MaxValue;
            _mysqlCommand.CommandText = "SELECT data FROM assets WHERE id = '"+ _id +"'";

            MySqlDataReader _commandReader = _mysqlCommand.ExecuteReader();

            while (_commandReader.Read())
            {
                _data = _commandReader.GetString(0);
            }

            _commandReader.Close();

            return _data;
        }
    }
}
