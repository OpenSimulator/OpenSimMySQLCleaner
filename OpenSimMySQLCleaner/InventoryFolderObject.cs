﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenSimMySQLCleaner
{
    class InventoryFolderObject
    {
        public String ID = String.Empty;
        public String ParentFolder = String.Empty;
        public String Avatar = String.Empty;
        public bool Found = false;

        public InventoryFolderObject(String _id, String _parent, String _agent)
        {
            ID = _id;
            ParentFolder = _parent;
            Avatar = _agent;
        }
    }
}
