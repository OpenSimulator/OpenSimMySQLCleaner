﻿using OpenMetaverse;
using OpenMetaverse.Assets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace OpenSimMySQLCleaner
{
    class IDTools
    {
        public static void addToList(ref List<String> _list, List<String> _input)
        {
            foreach (String _entry in _input)
            {
                addToList(ref _list, _entry);
            }
        }

        public static void addToList(ref List<String> _list, List<TaskInventoryItemElement> _input)
        {
            foreach (TaskInventoryItemElement _entry in _input)
            {
                addToList(ref _list, _entry.AssetID.UUID);
            }
        }

        public static void addToList(ref List<String> _list, String _input)
        {
            if (_input == "00000000-0000-0000-0000-000000000000")
                return;

            if (_input == null)
                return;

            if (!_list.Contains(_input))
                _list.Add(_input);
        }

        private static List<String> getUUIDListFromTexturEntry(String _base64)
        {
            List<String> _ausgabe = new List<string>();
            try
            {
                PrimObject obj = new PrimObject();

                byte[] teData = Convert.FromBase64String(_base64);
                obj.Textures = new Primitive.TextureEntry(teData, 0, teData.Length);

                foreach (Primitive.TextureEntryFace _face in obj.Textures.FaceTextures)
                {
                    if (_face != null)
                    {
                        _ausgabe.Add(_face.TextureID.ToString());
                    }
                }
            }
            catch
            {

            }
            return _ausgabe;
        }

        public static List<String> getUUIDListFromString(String _xml)
        {
            AppDomain.CurrentDomain.SetData("REGEX_DEFAULT_MATCH_TIMEOUT", TimeSpan.FromSeconds(10));
            List<String> _ausgabe = new List<string>();
            String _regex = @"[0-9A-Za-z]{8}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{12}";

            try
            {
                Regex _suche = new Regex(_regex);
                MatchCollection _funde = _suche.Matches(_xml);

                foreach (Match _f in _funde)
                {
                    String _value = _f.Value.ToUpper().Trim();
                    if (_value.Length < 75 && _value.Length > 10)
                    {
                        if (!_ausgabe.Contains(_value))
                        {
                            _ausgabe.Add(_value);
                        }
                    }
                }
            }
            catch (Exception _e)
            {
                Console.WriteLine(_e.ToString());
                Thread.Sleep(1000);
            }

            return _ausgabe;
        }


        public static List<String> getIDListFromSceneObjectGroup(SceneObjectGroup _group)
        {
            List<String> _returnList = new List<string>();

            if (_group.RootPart.SceneObjectPart.CollisionSound != null)
            {
                if (_group.RootPart.SceneObjectPart.CollisionSound.UUID != null)
                    addToList(ref _returnList, _group.RootPart.SceneObjectPart.CollisionSound.UUID);
            }

            if (_group.RootPart.SceneObjectPart.SoundID != null)
            {
                if (_group.RootPart.SceneObjectPart.SoundID.UUID != null)
                    addToList(ref _returnList, _group.RootPart.SceneObjectPart.SoundID.UUID);
            }

            if (_group.RootPart.SceneObjectPart.Shape.SculptTexture != null)
            {
                if (_group.RootPart.SceneObjectPart.Shape.SculptTexture.UUID != null)
                    addToList(ref _returnList, _group.RootPart.SceneObjectPart.Shape.SculptTexture.UUID);
            }

            if (_group.RootPart.SceneObjectPart.Shape != null)
            {
                if (_group.RootPart.SceneObjectPart.Shape.TextureEntry != null)
                    addToList(ref _returnList, getUUIDListFromTexturEntry(_group.RootPart.SceneObjectPart.Shape.TextureEntry));
            }

            if (_group.RootPart.SceneObjectPart.TaskInventory != null)
                addToList(ref _returnList, _group.RootPart.SceneObjectPart.TaskInventory);

            if (_group.OtherParts != null)
            {
                foreach (PartElement _sope in _group.OtherParts)
                {
                    if (_sope.SceneObjectPart.CollisionSound != null)
                    {
                        if (_sope.SceneObjectPart.CollisionSound.UUID != null)
                            addToList(ref _returnList, _sope.SceneObjectPart.CollisionSound.UUID);
                    }

                    if (_sope.SceneObjectPart.SoundID != null)
                    {
                        if (_sope.SceneObjectPart.SoundID.UUID != null)
                            addToList(ref _returnList, _sope.SceneObjectPart.SoundID.UUID);
                    }

                    if (_sope.SceneObjectPart.Shape.SculptTexture != null)
                    {
                        if (_sope.SceneObjectPart.Shape.SculptTexture.UUID != null)
                            addToList(ref _returnList, _sope.SceneObjectPart.Shape.SculptTexture.UUID);
                    }

                    if (_sope.SceneObjectPart.Shape != null)
                    {
                        if (_sope.SceneObjectPart.Shape.TextureEntry != null)
                            addToList(ref _returnList, getUUIDListFromTexturEntry(_sope.SceneObjectPart.Shape.TextureEntry));
                    }

                    if (_sope.SceneObjectPart.TaskInventory != null)
                        addToList(ref _returnList, (_sope.SceneObjectPart.TaskInventory));
                }
            }

            return _returnList;
        }
    }
}
