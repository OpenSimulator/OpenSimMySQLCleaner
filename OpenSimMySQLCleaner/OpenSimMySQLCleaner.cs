﻿using OpenMetaverse;
using OpenMetaverse.Assets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenSimMySQLCleaner
{
    class OpenSimMySQLCleaner
    {
        public static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream, Encoding.Unicode);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        private static List<String> getAllSubIDS(ref List<AssetObject> _allAssets, AssetObject _asset)
        {
            List<String> _return = new List<string>();
            if (_asset != null)
            {
                if (_asset.Parsed == false && _asset.Found == true)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SceneObjectGroup));

                    String _data = MySQLClient.getAssetData(_asset.ID);

                    if (_asset.Typ == 6)
                    {
                        Stream _stream = GenerateStreamFromString(_data);
                        SceneObjectGroup _objectGroup = null;

                        try
                        {
                            _objectGroup = (SceneObjectGroup)serializer.Deserialize(_stream);
                        }catch
                        {
                            foreach (String _id in IDTools.getUUIDListFromString(_data))
                            {
                                IDTools.addToList(ref _return, _id);
                            }
                        }

                        if(_objectGroup != null)
                        {
                            foreach (String _id in IDTools.getIDListFromSceneObjectGroup(_objectGroup))
                            {
                                IDTools.addToList(ref _return, _id);
                            }
                        }
                    }
                    else
                    {
                        foreach (String _id in IDTools.getUUIDListFromString(_data))
                        {
                            IDTools.addToList(ref _return, _id);
                        }
                    }
                }
            }


            return _return;
        }

        static void Main(string[] args)
        {
            MySQLClient.connect();

            List<AssetObject> _allAssets = MySQLClient.getAllAssetObjects();
            List<InventoryObject> _allInventorys = MySQLClient.getAllInventoryObjects();
            List<InventoryFolderObject> _allInventorysFolders = MySQLClient.getAllInventoryFolders();
            List<UserAccountObject> _allGridUsers = MySQLClient.getAllGridUsers();

            //Durchsuche die inventar folders solange bis keine neuen mehr gefunden werden.
            bool _startAgainToLookInFolders = true;
            while (_startAgainToLookInFolders)
            {
                _startAgainToLookInFolders = false;
                Console.WriteLine("Start folder run.");

                foreach (InventoryFolderObject _folder in _allInventorysFolders)
                {
                    if(_folder.Found == false && _folder.ParentFolder == "00000000-0000-0000-0000-000000000000")
                    {
                        UserAccountObject _avatar = _allGridUsers.Find(x => x.ID == _folder.Avatar);

                        if (_avatar != null)
                        {
                            _startAgainToLookInFolders = true;
                            _folder.Found = true;
                            Console.WriteLine("Found inventory root folder " + _folder.ID + ".");
                        }
                    }else
                    {
                        if(_folder.Found == false)
                        {
                            InventoryFolderObject _parentFolder = _allInventorysFolders.Find(x => x.ID == _folder.ParentFolder);

                            if (_parentFolder != null)
                            {
                                if (_parentFolder.Found == true)
                                {
                                    UserAccountObject _avatar = _allGridUsers.Find(x => x.ID == _folder.Avatar);

                                    if (_avatar != null)
                                    {
                                        _startAgainToLookInFolders = true;
                                        _folder.Found = true;
                                        Console.WriteLine("Found inventory folder " + _folder.ID + ".");
                                    }
                                }
                            }
                        }
                    }
                }
            }

            int _assetsInUse = 0;
            int _curinvitem = 1;

            //Markiere alle Assets die im Inventar sind
            foreach (InventoryObject _invObj in _allInventorys)
            {
                InventoryFolderObject _invFolder = _allInventorysFolders.Find(x => x.ID == _invObj.Parent);

                if(_invFolder != null)
                {
                    if(_invFolder.Found == true)
                    {
                        AssetObject _find = _allAssets.Find(x => x.ID == _invObj.AssetID);

                        if (_find != null && _find.ID != "00000000-0000-0000-0000-000000000000")
                        {
                            _find.Found = true;

                            List<String> _subIDs = getAllSubIDS(ref _allAssets, _find);
                            _find.Parsed = true;

                            foreach (String _id in _subIDs)
                            {
                                AssetObject _findSubAsset = _allAssets.Find(x => x.ID == _id);
                                if (_findSubAsset != null)
                                {
                                    if (_findSubAsset.Found == false)
                                    {
                                        _findSubAsset.Found = true;
                                        _assetsInUse++;
                                    }
                                }
                            }

                            Console.WriteLine("Found asset and " + _subIDs.Count + " subs at item " + _find.ID + " ( " + _curinvitem + " / " + _allInventorys.Count + ")");
                        }
                    }
                }

                _curinvitem++;
            }

            //Durchsuche die Asset liste solange bis keine Assets gefunden wurden die gefunden wurden aber noch nicht geparst wurden.
            bool _startAgain = true;
            while(_startAgain)
            {
                _startAgain = false;
                Console.WriteLine("Start asset run.");

                foreach (AssetObject _assetObj in _allAssets)
                {
                    if(_assetObj.Found == true && _assetObj.Parsed == false)
                    {
                        List<String> _subIDs = getAllSubIDS(ref _allAssets, _assetObj);
                        _assetObj.Parsed = true;

                        foreach (String _id in _subIDs)
                        {
                            AssetObject _findSubAsset = _allAssets.Find(x => x.ID == _id);
                            if (_findSubAsset != null)
                            {
                                if(_findSubAsset.Found == false)
                                {
                                    _findSubAsset.Found = true;
                                    _startAgain = true;
                                    _assetsInUse++;
                                }
                            }
                        }

                        Console.WriteLine("Found " + _subIDs.Count + " subs in asset " + _assetObj.Name + ". ("+ _assetsInUse + " / "+ _allAssets.Count + ")");
                    }
                }
            }


            //Generiere MySQL command file
            String ausgabe = "";
            foreach(AssetObject _ao in _allAssets)
            {
                if(_ao.Found)
                {
                    ausgabe += "UPDATE assets SET cleanupFound = '1' WHERE id = '" + _ao.ID + "'\n\r";
                }
                else
                {
                    ausgabe += "DELETE FROM assets WHERE id = '" + _ao.ID + "'\n\r";
                }
            }

            File.WriteAllText("sql.sql", ausgabe);

            Console.WriteLine("============================");
            Console.WriteLine("Found " + _allAssets.Count + " assets. (In use: "+ _assetsInUse + ")");
            Console.WriteLine("Found " + _allInventorys.Count + " inventory items.");
            Console.ReadLine();
        }
    }
}
