﻿using OpenMetaverse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenSimMySQLCleaner
{
    class AssetObject
    {
        public String ID = String.Empty;
        public String Name = String.Empty;
        public int Typ = 0;
        public bool Found = false;
        public bool Parsed = false;

        public AssetObject(String _id, String _name, int _typ)
        {
            ID = _id;
            Name = _name;
            Typ = _typ;
        }
    }
}
